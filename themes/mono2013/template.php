<?php
/**
 * @file
 * Sets up various things for the theme.
 */

drupal_add_js(drupal_get_path('theme', 'mono2013') .'/js/vegas/jquery.vegas.js', 'file');
//drupal_add_js(drupal_get_path('theme', 'mono2013') .'/js/jquery.equalheights.js', 'file');
drupal_add_css(drupal_get_path('theme', 'mono2013') .'/js/vegas/jquery.vegas.css', 'file');
//drupal_add_js('https://www.gstatic.com/swiffy/v5.1/runtime.js', 'external');
//drupal_add_js(drupal_get_path('theme', 'mono2013') .'/js/logo_swiffy.js', 'file');
drupal_add_js(drupal_get_path('theme', 'mono2013') .'/js/mono2013.js', 'file');
//drupal_add_css('http://fonts.googleapis.com/css?family=Caesar+Dressing', 'external');
drupal_add_css('http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css', 'external');

/**
 * Implementation of hook_menu_link().
 * This is done mainly to prevent Bootstrap's dropdown implementation.
 */

function mono2013_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    // Ad our own wrapper
    unset($element['#below']['#theme_wrappers']);
    $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';
    //$element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
    //$element['#localized_options']['attributes']['data-toggle'] = 'dropdown';

    // Check if this element is nested within another
    if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {
      // Generate as dropdown submenu
      $element['#attributes']['class'][] = 'dropdown-submenu';
    }
    else {
      // Generate as standard dropdown
      $element['#attributes']['class'][] = 'dropdown';
      $element['#localized_options']['html'] = TRUE;
      $element['#title'] .= '<span class="caret"></span>';
    }

    // Set dropdown trigger element to # to prevent inadvertant page loading with submenu click
    $element['#localized_options']['attributes']['data-target'] = '#';
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
