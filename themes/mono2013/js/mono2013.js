(function ($) {
function changeColor(e, speed){
  if (speed === 'fast') {
    speed = 500
  } else {
    speed = 1000
  }
  // Generate a new color
   var newcolor = 'rgb(' + (Math.floor(Math.random() * 256)) + ','
    + (Math.floor(Math.random() * 256)) + ','
    + (Math.floor(Math.random() * 256)) + ')';

  // Use the color-animation plugin to animate the change of color
  e.animate(
    {
      color: newcolor
    },
    {
      // duration for the change
      duration: speed,
      // When the animation is complete, the changeColor-function is called again
      complete: function(){
        changeColor(e);
      },
      // This makes sure the animation is not "enqueued", so it won't block
      // other animations on this element
      queue: false
    }
  );
}

  $(document).ready(function(){
    var height;
    var number = Math.floor((Math.random() * 2));
    console.log(number);
    $.vegas({
      src:'/sites/all/themes/mono2013/img/' + number + '.png',
      complete: function() {
        //height = $(this).height();
        //width = $(this).width();
        //var rand_x = Math.floor((Math.random() * width) / 3);
        //var rand_y = Math.floor((Math.random() * height) / 3);
        //$('.vegas-background').position({'my': 'top left', 'at': 'bottom left', 'of': 'html'});
       /* $('.vegas-background').stop().animate({left: -rand_x / 3}, {duration: 10000, easing: 'linear', queue: false})
          .animate({top: -rand_y / 3}, {duration: 10000, easing: 'linear', queue: false});
        $('nav .nav li').hover(function(){
          var pos = $('.vegas-background').position();
          var rand = Math.floor(Math.random()*4);
          if (rand === 0) {
            $('.vegas-background').stop().animate({left: -width / 3}, {duration: 10000, easing: 'linear', queue: false});
           // console.dir({'direction': 'left', 'rand' : rand});
          } else if (rand === 1) {
            $('.vegas-background').stop().animate({left: 0}, {duration: 10000, easing: 'linear', queue: false});
          //  console.dir({'direction': 'right', 'rand' : rand});
          } else if (rand === 2) {
            $('.vegas-background').stop().animate({top: -height / 3}, {duration: 10000, easing: 'linear', queue: false});
          //  console.dir({'direction': 'up', 'rand' : rand});
          } else {
            $('.vegas-background').stop().animate({top: 0}, {duration: 10000, easing: 'linear', queue: false});
          }
        },
        function() {
          $('.vegas-background').stop();
        }
        );*/
      }
    });
    $.vegas('overlay', {
      src:'/sites/all/themes/mono2013/img/overlay04.png'
    });
    //var stage = new swiffy.Stage(document.getElementById('logo-container'), swiffyobject);
    //stage.start();
    $('#fb-button').hover(function(){changeColor($(this));}, function(){$(this).stop().animate({color: '#3B5998'});});
    $('#tw-button').hover(function(){changeColor($(this));}, function(){$(this).stop().animate({color: '#4099FF'});});
    $('nav .nav li a').hover(function(){changeColor($(this), 'fast');}, function(){$(this).stop().animate({color: '#FFFFFF'});});
  });
})(jQuery)
